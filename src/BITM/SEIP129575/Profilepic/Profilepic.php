<?php
namespace App\BITM\SEIP129575\Profilepic;
use App\BITM\SEIP129575\Message\Message;
use App\BITM\SEIP129575\Utility\Utility;

class Profilepic{
    public $id="";
    public $name="";
    public $image_name="";
    public $deleted_at;
    public $status;
    public $conn;

    public function __construct(){
        $this->conn= mysqli_connect("localhost","root","","labxm7b20") or die("Database connection establish failed");
    }

    public function prepare($data=Array())
    {
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("image", $data)) {
            $this->image_name = $data['image'];

        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        $query="INSERT INTO `labxm7b20`.`profilepic` (`name`, `images`) VALUES ('{$this->name}', '{$this->image_name}')";

        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class='alert alert-info' >Data has been stored successfully</div>");
            Utility::redirect('index.php');
        }
        else {
            Message::message("<div class='alert alert-danger'>Data has not been stored successfully</div>");
            Utility::redirect('index.php');
        }
    }

    public  function index(){
        $_allInfo=array();
        $query= "SELECT * FROM `profilepic` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_object($result)){
            $_allInfo[]=$row;
        }
        return $_allInfo;
    }

    public function view(){
        $query="SELECT * FROM `profilepic` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }
    public function update(){
        if(!empty($this->image_name)) {
            $query = "UPDATE `labxm7b20`.`profilepic` SET `name` = '{$this->name}', `images` = '{$this->image_name}' WHERE `profilepic`.`id` =" . $this->id;
        }else{
            $query = "UPDATE `labxm7b20`.`profilepic` SET `name` = '{$this->name}' WHERE `profilepic`.`id` =" . $this->id;
        }

        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class='alert alert-info'>Data has been updated successfully</div>");
            Utility::redirect('index.php');
        }
        else {
            Message::message("<div class='alert alert-danger'>Data has not been updated successfully</div>");
            Utility::redirect('index.php');
        }
    }

    public function showpic(){
        $query="SELECT * FROM `profilepic` WHERE `status`='active'";
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }

    public function makeactive(){
        $query = "UPDATE `labxm7b20`.`profilepic` SET `status` ='active' WHERE `profilepic`.`id` = " . $this->id;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
                    Message::message("<div class='alert alert-info'> <strong>Activated</strong> Active</div>");
                    Utility::redirect("index.php");
                } else {
                    Message::message("<div class='alert alert-warning'><strong>Deactivated</strong></div>");
                    Utility::redirect("index.php");
                }

    }
    public function makedeactive(){
        $query = "UPDATE `labxm7b20`.`profilepic` SET `status` ='deactive' WHERE `profilepic`.`id` = " . $this->id;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
                        Message::message("<div class='alert alert-warning'><strong>Alert!!</strong> Deactived </div>");
                        Utility::redirect("index.php");
                    } else {
                        Message::message("<div class='alert alert-danger'><strong>Alert!!</strong> Error</div>");
            Utility::redirect("index.php");
        }

    }



    public function delete(){
        $query= "DELETE FROM `labxm7b20`.`profilepic` WHERE `profilepic`.`id` = ".$this->id;

        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class='alert alert-info'>Data has been deleted successfully</div>");
            Utility::redirect('index.php');
        }
        else {
            Message::message("Data has not been deleted successfully");
            Utility::redirect('index.php');
        }

    }
    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `labxm7b20`.`profilepic` SET `deleted_at` =" . $this->deleted_at . " WHERE `profilepic`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class='alert alert-info''><strong>Deleted!</strong> Data has been trashed successfully. </div>");
                    Utility::redirect("index.php");
                } else {
                    Message::message(" <div class='alert alert-info'><strong>Deleted!</strong> Data has not been trashed successfully.</div>");
                    Utility::redirect("index.php");
                }

    }

    public function trashed()
    {
        $_allprofilepic = array();
        $query = "SELECT * FROM `profilepic` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allprofilepic[] = $row;
        }

        return $_allprofilepic;


    }

    public function recover()
    {

        $query = "UPDATE `labxm7b20`.`profilepic` SET `deleted_at` = NULL WHERE `profilepic`.`id` = " . $this->id;
        echo $query;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class='alert alert-info'>
              <strong>Deleted!</strong> Data has been recovered successfully.
            </div>");
                        Utility::redirect("index.php");
                    } else {
                        Message::message("
            <div class='alert alert-info'>
              <strong>Deleted!</strong> Data has not been recovered successfully.
            </div>");
                        Utility::redirect("index.php");
                    }

    }


}