<?php
session_start();
include_once ('../../../vendor/autoload.php');


use App\Bitm\SEIP129575\Profilepic\Profilepic;
use App\Bitm\SEIP129575\Utility\Utility;
use App\Bitm\SEIP129575\Message\Message;
$profilePicture = new Profilepic();
//$allInfo=$profilePicture ->index();
$profilePicture->prepare($_GET)->recover();