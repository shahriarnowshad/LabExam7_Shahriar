<?php
session_start();
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP129575\Profilepic\Profilepic;
use App\Bitm\SEIP129575\Utility\Utility;
use App\Bitm\SEIP129575\Message\Message;

$profilePicture = new Profilepic();
$singleInfo= $profilePicture->prepare($_POST)->view();
if((isset($_FILES['image']))&& !empty($_FILES['image']['name'])){
    $imageName=time().$_FILES['image']['name'];
    $temporaryLocation= $_FILES['image']['tmp_name'];
    move_uploaded_file($temporaryLocation,'../../../Resource/Images/'. $imageName);
    unlink('../../../Resource/Images/'.$singleInfo->images);
    $_POST['image']= $imageName;
}

$profilePicture->prepare($_POST)->update();




?>