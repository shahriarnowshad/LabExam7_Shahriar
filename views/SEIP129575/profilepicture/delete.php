<?php
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP129575\Profilepic\Profilepic;

$profilePicture= new Profilepic();
$singleInfo= $profilePicture->prepare($_GET)->view();
unlink($_SERVER['DOCUMENT_ROOT'].'/Shahriar_129575_labexam7/Resource/Images/'.$singleInfo->images);
$profilePicture->prepare($_GET)->delete();