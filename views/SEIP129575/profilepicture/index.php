<?php
session_start();
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP129575\Profilepic\Profilepic;
use App\Bitm\SEIP129575\Utility\Utility;
use App\Bitm\SEIP129575\Message\Message;
$profilePicture = new Profilepic();
$allInfo=$profilePicture ->index();
$show=$profilePicture->showpic();
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>All User Info</h2>

    <a href="create.php" class="btn btn-info" role="button">Add User Info</a> <a href="trashed.php" class="btn btn-primary" role="button">View Trashed Item</a><br><br>

    <div id="message">
        <?php
        if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
            echo Message::message();
        }
        ?>
    </div>
    <div class="panel">
        <div class="panel-heading" style="color:white; background-color: #1b6d85";>Profile Picture</div>
        <div class="panel-body">
            <img src="../../../Resource/Images/<?php echo $show->images?>" alt="image" height="100px" width="100px"></div>
    </div>


    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>SL#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Profile Picture</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach ($allInfo as $info){
                $sl++?>
                <tr>
                    <td><?php echo $sl?></td>
                    <td><?php echo $info->id ?></td>
                    <td><?php echo $info->name ?></td>
                    <td><img src="../../../Resource/Images/<?php echo $info->images?>" alt="image" height="100px" width="100px"></td>
                    <td><a href="active.php?id=<?php echo $info->id ?>" class="btn btn-info" id="activated" role="button">Make Active</a>
                        <a href="deactive.php?id=<?php echo $info->id ?>" class="btn btn-danger" id="deactivated" role="button">dective</a>
                        <a href="view.php?id=<?php echo $info->id ?>" class="btn btn-info" role="button">View</a>
                        <a href="edit.php?id=<?php echo $info->id ?>" class="btn btn-primary" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo $info->id ?>" class="btn btn-danger" role="button">Delete</a>
                        <a href="trash.php?id=<?php echo $info->id ?>" class="btn btn-warning" role="button">Trash</a>
                    </td>
                </tr>
            <?php }?>

            </tbody>
        </table>
    </div>
</div>
<script>
    $('#message').show().delay(3000).fadeOut();

    $(document).ready(function(){
//        $('#activated').onclick(function(){
//            $("#activated").hide();
//            $("#deactivated").show();
//        });
//
//        $('#deactivated').onclick(function(){
//            $("#activated").show();
//            $("#deactivated").hide();
//        });
    });

</script>

</body>
</html>

