<?php
session_start();
include_once ('../../../vendor/autoload.php');
//var_dump($_POST);


use App\Bitm\SEIP129575\Profilepic\Profilepic;
use App\Bitm\SEIP129575\Utility\Utility;
use App\Bitm\SEIP129575\Message\Message;
$profilePicture = new Profilepic();
$allInfo=$profilePicture ->trashed();




?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>All User Info</h2>

    <a href="index.php" class="btn btn-info" role="button">View all Info</a> <a href="trashed.php" class="btn btn-primary" role="button">View Trashed Item</a><br><br>

    <div id="message">
        <?php
        if((array_key_exists('message',$_SESSION))&& !empty($_SESSION['message'])) {
            echo Message::message();
        }
        ?>
    </div>



    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>SL#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Profile Picture</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach ($allInfo as $info){
                $sl++?>
                <tr>
                    <td><?php echo $sl?></td>
                    <td><?php echo $info->id ?></td>
                    <td><?php echo $info->name ?></td>
                    <td><img src="../../../Resource/Images/<?php echo $info->images?>" alt="image" height="100px" width="100px"></td>
                    <td><a href="delete.php?id=<?php echo $info->id ?>" class="btn btn-danger" role="button">Delete</a>
                        <a href="recover.php?id=<?php echo $info->id ?>" class="btn btn-info" role="button">Recover</a>
                    </td>
                </tr>
            <?php }?>

            </tbody>
        </table>
    </div>
</div>
<script>
    $('#message').show().delay(3000).fadeOut();

    $(document).ready(function(){
//        $('#activated').onclick(function(){
//            $("#activated").hide();
//            $("#deactivated").show();
//        });
//
//        $('#deactivated').onclick(function(){
//            $("#activated").show();
//            $("#deactivated").hide();
//        });
    });

</script>

</body>
</html>

