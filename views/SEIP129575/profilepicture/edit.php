<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP129575\Profilepic\Profilepic;

$profilePicture= new Profilepic();
$singleInfo= $profilePicture->prepare($_GET)->view();


?>




<!DOCTYPE html>
<html>
<head>
    <title>Image Uploader</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Profile</h2>
<!--    <ul class="list-group">-->
<!--        <li class="list-group-item-text">Name : --><?php //echo $singleInfo->name ?><!-- </li>-->
<!--        <li class="list-group-item-text"> <img src="../../../Resource/Images/--><?php //echo $singleInfo->images?><!--" alt="image" height="100px" width="100px"></li>-->
<!--    </ul>-->
        <form role="form" action="update.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label>Name:</label>
                <input type="hidden" name="id" value="<?php echo $singleInfo->id ?>">
                <input type="text" class="form-control" id="usr" name="name" value="<?php echo $singleInfo->name?>">
            </div>
            <div class="form-group">
                <label>Upload your profile picture:</label>
                <input type="file" class="form-control" id="pwd" name="image">
                <img src="../../../Resource/Images/<?php echo $singleInfo->images?>" alt="image" height="100px" width="100px">
                <input type="submit" value="Update">
            </div>
        </form>
</div>

</body>
</html>


