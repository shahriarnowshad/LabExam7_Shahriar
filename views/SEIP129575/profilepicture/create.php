
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Image Uploader</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Create Profile</h2>

    <form role="form" action="store.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <label>Upload your profile picture:</label>
            <input type="file" class="form-control"   name="image">
            <input type="Submit" value="Submit">
        </div>
    </form>
</div>

</body>
</html>


