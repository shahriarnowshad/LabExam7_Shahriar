<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP129575\Profilepic\Profilepic;

$profilePicture= new Profilepic();
$singleInfo= $profilePicture->prepare($_GET)->view();


?>

<!DOCTYPE html>
<html>
<head>
    <title>Image Uploader</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="../../../Resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Profile</h2>
    <ul class="list-group">
        <li class="list-group-item">Name : <?php echo $singleInfo->name ?> </li>
        <li class="list-group-item"> <img src="../../../Resource/Images/<?php echo $singleInfo->images?>" alt="image" height="100px" width="100px"></li>
    </ul>

</div>

</body>
</html>


