<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP129575\Utility\Utility;
use App\Bitm\SEIP129575\Profilepic\Profilepic;

if((isset($_FILES['image']))&& !empty($_FILES['image']['name'])){
    $imageName=time().$_FILES['image']['name'];
    $temporaryLocation= $_FILES['image']['tmp_name'];
    move_uploaded_file($temporaryLocation,'../../../Resource/Images/'. $imageName);
    $_POST['image']= $imageName;
}
$profilePicture= new Profilepic();
$profilePicture->prepare($_POST)->store();

